FROM scratch
ADD server  server
EXPOSE 3000
CMD ["./server"]
