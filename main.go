package main

import (
	"log"
	"net/http"

	"gitlab.com/eithel/brackets/strips"
)

func main() {

	http.HandleFunc("/", bracketsless)
	log.Fatal(http.ListenAndServe(":3000", nil))

}

func bracketsless(w http.ResponseWriter, r *http.Request) {
	str := r.RequestURI[1:]
	out := strips.BracketsLess(str)
	log.Printf("received %s - returned %s\n", str, out)
	w.Header().Add("Content-type", "text/html; charset=UTF-8")
	w.Write([]byte(out))
}
