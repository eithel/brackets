# B1 - Brackets

## 1 - Design and implement a library/package to remove external matching round brackets. Assume no pathological input is provided [eg. no "((abc)"]

Per questo esercizio ho scelto di implementarlo usando il linguaggio GO.

La libreria può essere utilizzata usando `go get -u gitlab.com/eithel/brackets/strips` e una volta importata si può richiamare i metodi esposti come `strips.BracketsLess(a string)`.

Il pacchetto ha anche un file main.go, per la funzionalità richiesta nel punto 5.

## 2- How can be reasonably sure the correctness of the solution?

I casi semplici sono gestiti all'inizio della funzione `BracketsLess` (strips.go:8) i quali includono:

- lunghezza minima
- la stringa deve iniziare per `(`, e terminare per `)`

Se la stringa supera i primi controlli, la funzione inizia a "contare" i caratteri ti tipo `(` e `)`.
Il counting è necessario per escludere le coppie di parentesi "interne" tipo `((ab) (cd))`, oppure i casi particolari tipo `(ab) (cd)`, dove effettivamente ci sono diverse parentesi ma non "esterne".

La funzione poi è abbastanza semplice: rimuove le parentesi iniziale e finale, e poi richiama se stessa, passando la substring.

Per essere completamente sicuri, forse è da cambiare la modalità di parsing della stringa, tokerizzando le parentesi e costruendo un albero e usando funzioni tipiche degli alberi.

La libreria inoltre è fornita di test.

## 3 - Discuss the space-time complexity of the solution (both in general and language related)

Poiché la funzione, al netto dei casi speciali, itera tutta la stringa, la funzione dipende dalla stringa in input.
Inoltre poiché richiama se stessa, passando una substringa la sua complessità aumenta in modo esponenziale.
Non è sicuramente una funzione efficente.

Questa è una soluzione abbastanza semplice ed è stata portata a terra in breve tempo usando pochissime funzionalità specifiche del linguaggio scelto, quindi è facilmente "portabile" in altre implementazioni.

## 4- What if a pathological input could be provided ?

La soluzione in questo caso supporta un input patologico. La prima implementazione non la supportava: leggendo la stringa da destra verso sinistra, riusciva ad individuare e semplificare stringhe del tipo `((abc)` ma non stringhe del tipo `(abc))`. E' stato implementato il controllo (strips.go:19) proprio per questi casi particolari.

## 5- Provide this solution as a service a user can call via HTTP

Il pacchetto ha un file `main.go` nel quale è definito un semplice http endpoint (**not production ready**).

E' presente anche un file `Makefile` che genera un'immagine Docker con l'endpoint esposto alla porta 3000. Il Makefile richiede che sia installato il compilatore `go` nella macchia. In questo modo l'immagine generata, che parte da scratch (vedi `Dockerfile`) è molto leggera (quindi favorisce il provisioning).

Una volta generata l'immagine, si può lanciare un container con il comando `docker run -d -p 3000:3000 servers/bracketsless:latest` e visitare la seguente url `http://localhost:3000/((ab)(cd))`.



