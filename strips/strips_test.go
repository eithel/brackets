package strips

import "testing"

func TestBracketsLess(t *testing.T) {
	type args struct {
		a string
	}
	tests := []struct {
		name string
		a    string
		want string
	}{
		{
			name: "(abc)",
			a:    "(abc)",
			want: "abc",
		},
		{
			name: "((abc))",
			a:    "((abc))",
			want: "abc",
		},
		{
			name: "(abc",
			a:    "(abc",
			want: "(abc",
		},
		{
			name: "",
			a:    "",
			want: "",
		},
		{
			name: "abc",
			a:    "abc",
			want: "abc",
		},
		{
			name: "()",
			a:    "()",
			want: "",
		},
		{
			name: "(ab) (cd)",
			a:    "(ab) (cd)",
			want: "(ab) (cd)",
		},
		{
			name: "((ab) (cd)",
			a:    "((ab) (cd))",
			want: "(ab) (cd)",
		},
		{
			name: "((ab)(cd))",
			a:    "((ab)(cd))",
			want: "(ab)(cd)",
		},
		{
			name: "((ab)",
			a:    "((ab)",
			want: "(ab",
		},
		{
			name: "ab(cd))",
			a:    "ab(cd)",
			want: "ab(cd)",
		},
		{
			name: "((abc))asd(as))",
			a:    "((abc))asd(as))",
			want: "((abc))asd(as))",
		},
		{
			name: "(((abc))asd(as)))",
			a:    "(((abc))asd(as)))",
			want: "((abc))asd(as))",
		},
		{
			name: "((abc)poi)",
			a:    "((abc)poi)",
			want: "(abc)poi",
		},
		{
			name: "(erf)aas)",
			a:    "(erf)aas)",
			want: "erf)aas",
		},
		{
			name: "(((()))",
			a:    "(((()))",
			want: "(",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BracketsLess(tt.a); got != tt.want {
				t.Errorf("BracketsLess() = %v, want %v", got, tt.want)
			}
		})
	}
}
