// Package strips provides some utils to work with string and remove extra chars.
package strips

// BracketsLess removes the external brackets "(" ")" of a given string
func BracketsLess(a string) string {

	// a string must have len > 2, start with (, finish with )
	if len(a) < 2 || a[0] != '(' || a[len(a)-1] != ')' {
		return a
	}

	count := 0
	positionClosed := -1
	findOnlyMoreClosed := false
	for i, s := range a {

		// when a "couple of brackets" is "closed" - a.k.a. we found '(' and ')' -,
		// we check if there is only more ')'.
		if positionClosed != -1 && count == 0 {
			findOnlyMoreClosed = true
		}

		switch s {
		case '(':
			if findOnlyMoreClosed {
				return a
			}
			count++
		case ')':
			positionClosed = i
			count--
		}

	}

	if positionClosed == len(a)-1 {
		// remove the 0 / len(a) parts of string and work with a smaller string
		return BracketsLess(a[1 : len(a)-1])
	}
	return a

}
